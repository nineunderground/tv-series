# -*- coding: utf-8 -*-
import sys
import os
import xbmcgui
import xbmcplugin
import xbmc
import urllib
import urlparse
import seriespepito
import lib.xml.ElementTree as XMLdecoder

xbmc.log(msg='INICIO DE PLUGIN')

base_url = sys.argv[0]
addon_handle = int(sys.argv[1]) # Variable con el numero de proceso
print sys.argv[2]
args = urlparse.parse_qs(sys.argv[2][1:])
print args
__channel__ = "seriespepito"

'''
Se le dice a XBMC que tipo de contenido es, por si el lo quiere visualizar
segun las preferencias del sistema de alguna manera en particular
'''
xbmcplugin.setContent(addon_handle, 'movies')

# Leemos el fichero XML
tree = XMLdecoder.parse('/storage/.xbmc/addons/plugin.video.tv.series/categories.xml')
root = tree.getroot()
listaCategorias = []

for child in root:
	listaCategorias.append(child.tag)

def build_url(query):
	"Devuelve una url estructurada"
	return base_url + '?' + urllib.urlencode(query)

def addfolder(nombre,channelname,accion,category="",thumbnailname="",thumbnail="",folder=True,level=0,url=""):
    if category == "":
        try:
            category = unicode( nombre, "utf-8" ).encode("iso-8859-1")
        except:
            pass
    
    #import xbmc
    #import xbmcgui
    #import xbmcplugin
    listitem = xbmcgui.ListItem( nombre , iconImage="DefaultFolder.png", thumbnailImage=thumbnail)
    itemurl = '%s?level=%s&link=%s&channel=%s&action=%s&category=%s' % ( sys.argv[ 0 ] , level, url, channelname , accion , category )
    xbmcplugin.addDirectoryItem( handle = int(sys.argv[ 1 ]), url = itemurl , listitem=listitem, isFolder=folder)

# Se recupera un argumento que nos dice si estamos en un nivel u otro
level = args.get('level', None)

# LEVEL 1 - MOSTRAR LOS GENEROS DISPONIBLES DEL FICHERO
if level is None:
    xbmc.log(msg='ELIGE TU CATEGORIA...')
    for x in listaCategorias:
    	url = build_url({'level': 1, 'categoria': x})
    	li = xbmcgui.ListItem(x, iconImage='DefaultFolder.png')
    	xbmcplugin.addDirectoryItem(handle=addon_handle, url=url,listitem=li, isFolder=True)

    xbmcplugin.endOfDirectory(addon_handle)

# LEVEL 2 - MOSTRAR SERIES DE LA CATEGORIA SELECCIONADA
elif level[0] == '1':
	catSelected = args.get('categoria', None)
	print catSelected
	xbmc.log(msg='ELIGE TU SERIE DE ' + catSelected[0])
	seriesCategoria = root.find(catSelected[0])
	for x in seriesCategoria:
		url = build_url({'level': 2, 'serieURL': x.text})
		li = xbmcgui.ListItem(x.get('nombre'), iconImage='DefaultFolder.png')
		xbmcplugin.addDirectoryItem(handle=addon_handle, url=url,listitem=li, isFolder=True)

	xbmcplugin.endOfDirectory(addon_handle)

elif level[0] == '2':
	xbmc.log(msg='MOSTRAR LOS CAPITULOS DE LA SERIE SELECCIONADA. URL=')
	from core.item import Item
	link = args.get('serieURL', None)
	enlace = link[0][1:-1] #Remove commas
	paginaUrl = Item(channel=__channel__, action="level_JUEGODETRONOS", title="Novedades", url=enlace, fanart="http://pelisalacarta.mimediacenter.info/fanart/seriespepito.jpg")
	
	itemlist = []
	itemlist = seriespepito.episodios(paginaUrl)

	for elemento in itemlist:
		addfolder(elemento.title , elemento.channel , elemento.action , thumbnail=elemento.thumbnail, folder=elemento.folder, level=3, url=elemento.url)
	
	#import xbmcplugin
	xbmcplugin.setPluginCategory( handle=int( sys.argv[ 1 ] ), category="" )
	xbmcplugin.addSortMethod( handle=int( sys.argv[ 1 ] ), sortMethod=xbmcplugin.SORT_METHOD_NONE )
	xbmcplugin.endOfDirectory( handle=int( sys.argv[ 1 ] ), succeeded=True )

	xbmc.log(msg='CAPITULOS CARGADOS!! :-)')

elif level[0] == '3':
	xbmc.log(msg='MOSTRAR ENLACES (DE STREAMCLOUD) DEL CAPITULO SELECCIONADO')

	link = args.get('link', None)
	from core.item import Item
	episode_item = Item(channel=__channel__, title="Capitulo", url=link[0], fanart="http://pelisalacarta.mimediacenter.info/fanart/seriespepito.jpg")
	mediaurls = seriespepito.findvideos(episode_item)

	episodeSPA	= False
	episodeVOS	= False
	episodeVO	= False

	for mediaurl in mediaurls:
		# Habria que filtrar solo servidores Streamcloud - Uno en VO, otro en VOS, y otro en Espanol
		if mediaurl.title.find('Streamcloud') > -1: # Cojo solo Streamcloud
			if mediaurl.title.find('Latino') > -1:
				continue
				# El latino no lo pongo
			if episodeVOS == False:
				if mediaurl.title.find('VOS)') > -1:
					addfolder(mediaurl.title , mediaurl.channel , mediaurl.action , thumbnail=mediaurl.thumbnail, folder=mediaurl.folder, level=4, url=mediaurl.url)
					episodeVOS = True
					#print mediaurl.url
			if episodeVO == False:
				if mediaurl.title.find('VO)') > -1:
					addfolder(mediaurl.title , mediaurl.channel , mediaurl.action , thumbnail=mediaurl.thumbnail, folder=mediaurl.folder, level=4, url=mediaurl.url)
					episodeVO = True
					#print mediaurl.url
			if episodeSPA == False:
				if mediaurl.title.find('Español)') > -1: # 00F1
					addfolder(mediaurl.title , mediaurl.channel , mediaurl.action , thumbnail=mediaurl.thumbnail, folder=mediaurl.folder, level=4, url=mediaurl.url)
					episodeSPA = True
					#print mediaurl.url

	xbmcplugin.setPluginCategory( handle=int( sys.argv[ 1 ] ), category="" )
	xbmcplugin.addSortMethod( handle=int( sys.argv[ 1 ] ), sortMethod=xbmcplugin.SORT_METHOD_NONE )
	xbmcplugin.endOfDirectory( handle=int( sys.argv[ 1 ] ), succeeded=True )

elif level[0] == '4':
	xbmc.log(msg='VER EL CAPITULO SELECCIONADO...')
	from core.item import Item
	mediaurl = args.get('link', None)
	link_item = Item(channel=__channel__, title="Capitulo", url=mediaurl[0], fanart="http://pelisalacarta.mimediacenter.info/fanart/seriespepito.jpg")
	elemento = seriespepito.play(link_item)
	finalLink = elemento[0]
	li = xbmcgui.ListItem('Juego de Tronos 1x01')
	# TO-DO Poner la info del capitulo
	li.setInfo('video', {'Title': 'Primer Capitulo', 'Genre': 'Fantasia'})
	reproductor = xbmc.Player()
	
	import servers.streamcloud as myServer
	video_urls = myServer.get_video_url(finalLink.url)
	if len(video_urls) > 0:
		reproductor.play( item=video_urls[0][1], listitem=li, windowed=False)

	'''
	IMAGE_PATH = xbmc.translatePath( os.path.join('/storage/.xbmc/addons/plugin.video.tv.series/resources/data/images','game_of_thrones.jpeg') ) # ruta de la imagen
	li = xbmcgui.ListItem('JUEGO DE TRONOS', iconImage=IMAGE_PATH) # Se crea una lista de objetos
	xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li) # XBMC pinta en pantalla el control de lista de objetos
	'''

xbmc.log(msg='FIN DE PLUGIN')