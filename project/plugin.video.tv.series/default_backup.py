import sys
import urllib
import urlparse
import xbmcgui
import xbmcplugin

'''
TVE 1 http://iphonelive.rtve.es/LA1_LV3_IPH/LA1_LV3_IPH.m3u8
Tve 2 http://iphonelive.rtve.es/LA2_LV3_IPH/LA2_LV3_IPH.m3u8
Tve 24h http://iphonelive.rtve.es/24H_LV3_IPH/24H_LV3_IPH.m3u8
http://iphone-andaluciatelevision.rtva.stream.flumotion.com/rtva/andaluciatelevision-iphone-multi/main.m3u8
http://cdn6.streamcloud.eu:8080/6lv75oxhdcoax3ptx32ijaxfukrhwereyn67v6lwslven2cau75cfoppx4/video.mp4
'''


base_url = sys.argv[0]
addon_handle = int(sys.argv[1])
args = urlparse.parse_qs(sys.argv[2][1:])

xbmcplugin.setContent(addon_handle, 'movies')

def build_url(query):
    return base_url + '?' + urllib.urlencode(query)

mode = args.get('mode', None)

if mode is None:
    url = build_url({'mode': 'folder', 'foldername': 'Folder One'})
    li = xbmcgui.ListItem('Folder One', iconImage='DefaultFolder.png')
    xbmcplugin.addDirectoryItem(handle=addon_handle, url=url,
                                listitem=li, isFolder=True)

    url = build_url({'mode': 'folder', 'foldername': 'Folder Two'})
    li = xbmcgui.ListItem('Folder Two', iconImage='DefaultFolder.png')
    xbmcplugin.addDirectoryItem(handle=addon_handle, url=url,
                                listitem=li, isFolder=True)

    xbmcplugin.endOfDirectory(addon_handle)

elif mode[0] == 'folder':
    foldername = args['foldername'][0]
    url = 'http://localhost/some_video.mkv'
    li = xbmcgui.ListItem(foldername + ' Video', iconImage='DefaultVideo.png')
    xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li)
    xbmcplugin.endOfDirectory(addon_handle)